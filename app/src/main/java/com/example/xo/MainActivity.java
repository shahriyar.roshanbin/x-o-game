package com.example.xo;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    public TextView clicked;
    public TextView display;
    public HashMap<Integer,TextView> buttons = new HashMap<Integer, TextView>();
    public String turn ="X";
    int counter=0;
    Game game =new  Game();
    ArrayList<Integer> winnerCheckList = new ArrayList<Integer>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        display =(TextView) findViewById(R.id.display);
        buttons .put(0,(TextView) findViewById(R.id.c0));
        buttons .put(1,(TextView) findViewById(R.id.c1));
        buttons .put(2,(TextView) findViewById(R.id.c2));
        buttons .put(3,(TextView) findViewById(R.id.c3));
        buttons .put(4,(TextView) findViewById(R.id.c4));
        buttons .put(5,(TextView) findViewById(R.id.c5));
        buttons .put(6,(TextView) findViewById(R.id.c6));
        buttons .put(7,(TextView) findViewById(R.id.c7));
        buttons .put(8,(TextView) findViewById(R.id.c8));




    }

    public void playTurn(View view) {
        switch (view.getId()) {
            case R.id.c0:
                clicked = (TextView) findViewById(R.id.c0);
                if (turn=="X"){
                    clicked.setText("X");
                    winnerCheck(0,0);
                    turn="O";
                }
                else{
                    clicked.setText("O");
                    winnerCheck(1,0);
                    turn="X";
                }

                break;
            case R.id.c1:
                clicked = (TextView) findViewById(R.id.c1);
                if (turn=="X"){
                    clicked.setText("X");
                    winnerCheck(0,1);
                    turn="O";
                }
                else{
                    clicked.setText("O");
                    winnerCheck(1,1);
                    turn="X";
                }
                break;
            case R.id.c2:
                clicked = (TextView) findViewById(R.id.c2);
                if (turn=="X"){
                    clicked.setText("X");
                    winnerCheck(0,2);
                    turn="O";
                }
                else{
                    clicked.setText("O");
                    winnerCheck(1,2);
                    turn="X";
                }
                break;
            case R.id.c3:
                clicked = (TextView) findViewById(R.id.c3);
                if (turn=="X"){
                    clicked.setText("X");
                    winnerCheck(0,3);
                    turn="O";
                }
                else{
                    clicked.setText("O");
                    winnerCheck(1,3);
                    turn="X";
                }
                break;
            case R.id.c4:
                clicked = (TextView) findViewById(R.id.c4);
                if (turn=="X"){
                    clicked.setText("X");
                    winnerCheck(0,4);
                    turn="O";
                }
                else{
                    clicked.setText("O");
                    winnerCheck(1,4);
                    turn="X";
                }
                break;
            case R.id.c5:
                clicked = (TextView) findViewById(R.id.c5);
                if (turn=="X"){
                    clicked.setText("X");
                    winnerCheck(0,5);
                    turn="O";
                }
                else{
                    clicked.setText("O");
                    winnerCheck(1,5);
                    turn="X";
                }
                break;
            case R.id.c6:
                clicked = (TextView) findViewById(R.id.c6);
                if (turn=="X"){
                    clicked.setText("X");
                    winnerCheck(0,6);
                    turn="O";
                }
                else{
                    clicked.setText("O");
                    winnerCheck(1,6);
                    turn="X";
                }
                break;
            case R.id.c7:
                clicked = (TextView) findViewById(R.id.c7);
                if (turn=="X"){
                    clicked.setText("X");
                    winnerCheck(0,7);
                    turn="O";
                }
                else{
                    clicked.setText("O");
                    winnerCheck(1,7);
                    turn="X";
                }
                break;
            case R.id.c8:
                clicked = (TextView) findViewById(R.id.c8);
                if (turn=="X"){
                    clicked.setText("X");
                    winnerCheck(0,8);
                    turn="O";
                }
                else{
                    clicked.setText("O");
                    winnerCheck(1,8);
                    turn="X";
                }
                break;



        }

    }
    public void winnerCheck(int player,int place){
        if (player==0){
            display.setText("O");
        }else{
            display.setText("X");
        }
        winnerCheckList = game.bind(player,place);
        if (winnerCheckList.get(0)!=999){
            if ( winnerCheckList.get(0)==0){
                display.setText("Player X won");

            }
            else{
                display.setText("Player O won");

            }
            for (int i=0;i<9;i++){
                buttons.get(i).setEnabled(false);
        }
            winnerCheckList.remove(0);
            display.setBackgroundColor(Color.rgb(0,255,0));

            for (int i =0;i<3;i++){
                buttons.get(winnerCheckList.get(i)).setBackgroundColor(Color.rgb(0,255,0));
            }
        }
        else{
            counter++;
            if (counter == 9){
                for (int i=0;i<9;i++){
                    buttons.get(i).setEnabled(false);
                }
                display.setText("No one won");

            }


        }
    }
    public void newGame(View view){
        display.setBackgroundColor(Color.rgb(255,255,255));
        display.setText("Start to your move");

        turn ="X";
        counter=0;
        winnerCheckList.clear();
        game.newGame();
        for (int i=0;i<9;i++){
            buttons.get(i).setEnabled(true);
            buttons.get(i).setText(null);
            buttons.get(i).setAlpha(0.5f);

            buttons.get(i).setBackgroundColor(Color.rgb(255,255,255));
        }
    }
}

class Game {
    ArrayList<ArrayList<Boolean>> players = new ArrayList<ArrayList<Boolean>>();

    public Game() {

        newGame();
    }

    public void newGame() {
        players.clear();
        players.add(new ArrayList<Boolean>());
        players.add(new ArrayList<Boolean>());

        for (int i = 0; i < 9; i++) {

            players.get(0).add(false);
            players.get(1).add(false);

        }

    }

    public ArrayList<Integer> bind(int player, int place) {
        ArrayList<Integer> winner = new ArrayList<Integer>();
        winner.add(999);
        winner.add(0);
        winner.add(0);
        winner.add(0);

        players.get(player).set(place, true);
        ArrayList<Boolean> playerInstance = players.get(player);
        if (

                        playerInstance.get(0) == true &&
                                playerInstance.get(1) == true &&
                                playerInstance.get(2) == true

                ) {
            winner.clear();
            winner.add(player);
            winner.add(0);
            winner.add(1);
            winner.add(2);



        } else if
                        (
                                playerInstance.get(3) == true &&
                                        playerInstance.get(4) == true &&
                                        playerInstance.get(5) == true
                        ){
            winner.clear();
            winner.add(player);
            winner.add(3);
            winner.add(4);
            winner.add(5);


        }else if
                        (
                                playerInstance.get(6) == true &&
                                        playerInstance.get(7) == true &&
                                        playerInstance.get(8) == true
                        ){
            winner.clear();
            winner.add(player);
            winner.add(6);
            winner.add(7);
            winner.add(8);


        }else if
                        (
                                playerInstance.get(0) == true &&
                                        playerInstance.get(3) == true &&
                                        playerInstance.get(6) == true
                        ){
            winner.clear();
            winner.add(player);
            winner.add(0);
            winner.add(3);
            winner.add(6);

        }else if
                        (
                                playerInstance.get(1) == true &&
                                        playerInstance.get(4) == true &&
                                        playerInstance.get(7) == true
                        ){
            winner.clear();
            winner.add(player);
            winner.add(1);
            winner.add(4);
            winner.add(7);

        }else if
                        (
                                playerInstance.get(2) == true &&
                                        playerInstance.get(5) == true &&
                                        playerInstance.get(8) == true
                        ){
            winner.clear();
            winner.add(player);
            winner.add(2);
            winner.add(5);
            winner.add(8);

        }else if
                        (
                                playerInstance.get(2) == true &&
                                        playerInstance.get(4) == true &&
                                        playerInstance.get(6) == true
                        ){
            winner.clear();
            winner.add(player);
            winner.add(2);
            winner.add(4);
            winner.add(6);

        }else if
                        (
                                playerInstance.get(0) == true &&
                                        playerInstance.get(4) == true &&
                                        playerInstance.get(8) == true
                        )
        {
            winner.clear();
            winner.add(player);
            winner.add(0);
            winner.add(4);
            winner.add(8);
        }


            return winner;
    }

}

